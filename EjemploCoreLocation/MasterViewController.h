//
//  MasterViewController.h
//  EjemploCoreLocation
//
//  Created by José Manuel Fierro Conchouso on 21/02/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
